Most popular CPU, RAM and battery in Convertibles
=================================================

See more info in the [README](https://github.com/linuxhw/DMI).

Contents
--------

1. [ Devices in Convertibles ](#devices)
   * [ Cpu ](#cpu)
   * [ Memory ](#memory)
   * [ Battery ](#battery)

Devices
-------

Count  — number of computers with this device installed,
Probe  — latest probe ID of this device.

### Cpu

| MFG        | Name                           | GHz  | Count | Probe      |
|------------|--------------------------------|------|-------|------------|
| Intel      | Core i7-8550U                  | 1.80 | 6     | FD6A48B8F8 |
| Intel      | Core i7-7500U                  | 2.70 | 5     | 3AC5B5C4AD |
| Intel      | Atom x5-Z8350                  | 1.44 | 4     | FAB48926D9 |
| AMD        | Ryzen 5 2500U with Radeon V... |      | 3     | 9CAC1E8AA6 |
| Intel      | Core i5-8250U                  | 1.60 | 2     | 46D1308039 |
| Intel      | Core i7-8565U                  | 1.80 | 2     | 74DD313167 |
| AMD        | FX-9800P RADEON R7, 12 COMP... |      | 1     | D103A0F533 |
| Intel      | Celeron N4100                  | 1.10 | 1     | 11EC0315D7 |
| Intel      | Core i7-8650U                  | 1.90 | 1     | 0818236221 |
| Intel      | Core i3-7100U                  | 2.40 | 1     | C3AA237F35 |
| Intel      | Core i5-7200U                  | 2.50 | 1     | A295EE15E6 |
| Intel      | Core i5-7Y57                   | 1.20 | 1     | 5EA0111CED |
| Intel      | Core i7-7600U                  | 2.80 | 1     | 08EE9F4224 |
| Intel      | Core i7-8705G                  | 3.10 | 1     | 07A9851CD6 |
| Intel      | Celeron N2840                  | 2.16 | 1     | 07B5ECDCCD |
| Intel      | Core i3-5010U                  | 2.10 | 1     | 62A9EF950E |
| Intel      | Core i7-5500U                  | 2.40 | 1     | D9CA1DBE13 |
| Intel      | Atom x5-Z8300                  | 1.44 | 1     | 00D3EF2D8A |
| Intel      | Celeron N3060                  | 1.60 | 1     | BE4128010B |
| Intel      | Core i3-6006U                  | 2.00 | 1     | 5B8D494C04 |
| Intel      | Core i5-6200U                  | 2.30 | 1     | 2DE25CD685 |
| Intel      | Core i7-6600U                  | 2.60 | 1     | 3C1A4327F5 |
| Intel      | Pentium 4405U                  | 2.10 | 1     | 4ACED2B19A |

### Memory

| MFG        | Name               | Size     | Type | MT/s | Count | Probe      |
|------------|--------------------|----------|------|------|-------|------------|
| SK Hynix   | HMA81GS6AFR8N-U... | 8192 MB  | DDR4 | 2400 | 4     | 6D2724E820 |
| SK Hynix   | Module DIMM        | 4096 MB  | DDR3 | 1066 | 3     | 43E4578544 |
| Samsung    | K4EBE304EB-EGCG... | 8192 MB  |      | 2133 | 3     | 42FB487F36 |
| SK Hynix   | HMAA51S6AMR6N-U... | 8192 MB  | DDR4 | 2400 | 2     | FD6A48B8F8 |
|            | 123456789012345... | 2048 MB  |      | 2133 | 1     | 11EC0315D7 |
| SK Hynix   | HMA41GS6AFR8N-T... | 8192 MB  | DDR4 | 933  | 1     | D103A0F533 |
| SK Hynix   | HMA851S6CJR6N-V... | 4096 MB  | DDR4 | 2400 | 1     | 9CAC1E8AA6 |
| SK Hynix   | H9CCNNNBLTALAR-... | 4096 MB  |      | 1600 | 1     | D9CA1DBE13 |
| SK Hynix   | Module DIMM        | 2048 MB  | DDR3 | 1600 | 1     | 00D3EF2D8A |
| SK Hynix   | Module DIMM        | 4096 MB  | DDR3 | 1600 | 1     | FAB48926D9 |
|            | K4E6E304EB-EGCF... | 4096 MB  |      | 1867 | 1     | 5EA0111CED |
| Kingston   | 9905624-044.A00... | 8192 MB  | DDR4 | 2400 | 1     | 45B8A4C9DF |
| Kingston   | HP24D4S7S8MBP-8... | 8192 MB  | DDR4 | 2400 | 1     | 46D1308039 |
| Kingston   | Module SODIMM      | 4096 MB  | DDR4 | 2400 | 1     | E5DA00D166 |
| Micron     | 4ATS1G64HZ-2G6E... | 16384 MB | DDR4 | 2400 | 1     | 07A9851CD6 |
| Micron     | 8ATF1G64HZ-2G6E... | 8192 MB  | DDR4 | 2667 | 1     | 45B8A4C9DF |
| Micron     | 8KTF51264HZ-1G6... | 4096 MB  | DDR3 | 1600 | 1     | 62A9EF950E |
| Micron     | MT41K256M16HA-1... | 2048 MB  | DDR3 | 1333 | 1     | 07B5ECDCCD |
| Micron     | Module SODIMM      | 8192 MB  | DDR4 | 2400 | 1     | E5DA00D166 |
|            | Module Row Of C... | 8192 MB  |      | 1867 | 1     | 08EE9F4224 |
| Ramaxel    | RMSA3260MB78HAF... | 8192 MB  | DDR4 | 2133 | 1     | A295EE15E6 |
| Samsung    | K4AAG165WB-MCRC... | 8192 MB  | DDR4 | 2400 | 1     | 74DD313167 |
| Samsung    | K4EBE304EB-EGCF... | 8192 MB  |      | 1867 | 1     | 7DA5C4A4FA |
| Samsung    | M471A1K44BM0-CR... | 8192 MB  | DDR4 | 2400 | 1     | 6D2724E820 |
| Samsung    | M471A5143EB0-CP... | 4096 MB  | DDR4 | 2133 | 1     | 5B8D494C04 |
| Samsung    | M471A5244CB0-CR... | 4096 MB  | DDR4 | 2400 | 1     | C3AA237F35 |
| Samsung    | M471A5244CB0-CT... | 4096 MB  | DDR4 | 2667 | 1     | 3A1D39F7D3 |
| Samsung    | M471B5173EB0-YK... | 4096 MB  | DDR3 | 1600 | 1     | BE4128010B |
| SK Hynix   | H9CCNNNBJTALAR-... | 4096 MB  |      | 2133 | 1     | 831911B060 |
| SK Hynix   | H9CCNNNCLJALAR-... | 8192 MB  |      | 1867 | 1     | 3AC5B5C4AD |
| SK Hynix   | HMA451S6AFR8N-T... | 4096 MB  | DDR4 | 2133 | 1     | 4ACED2B19A |
| SK Hynix   | HMA81GS6CJR8N-V... | 8192 MB  | DDR4 | 2667 | 1     | D838D80F49 |

### Battery

| MFG        | Name          | Wh    | Type    | Count | Probe      |
|------------|---------------|-------|---------|-------|------------|
| Celxpert   | PABAS0241231  | 35.3  | Li-ion  | 2     | 5B8D494C04 |
| Compal     | PABAS0241231  | 30.9  | Li-ion  | 2     | 07B5ECDCCD |
| CPT-COS    | L17C4PB0      | 45.5  | Li-poly | 2     | D838D80F49 |
| Hewlett... | Primary       | 61    | Li-ion  | 2     | 831911B060 |
| 313-54-41  | MB04055XL     | 28.6  | Li-ion  | 1     | D103A0F533 |
| 313-83-24  | LE03048XL     | 42.4  | Li-ion  | 1     | 62A9EF950E |
| 333-1C-... | KC04053XL     | 53.2  | Li-ion  | 1     | 9CAC1E8AA6 |
| 333-42-2C  | LK03055XL     | 48.7  | Li-ion  | 1     | 3E13F3F3B8 |
| 333-54-2A  | LE03048XL     | 40.9  | Li-ion  | 1     | 2DE25CD685 |
| 333-54-... | LK03052XL     | 55.5  | Li-ion  | 1     | 45B8A4C9DF |
| 333-54-... | BK03041XL     | 36.8  | Li-ion  | 1     | 46D1308039 |
| ASUSTeK    | ASUS          | 50.1  | Li-ion  | 1     | 42FB487F36 |
|            |               | 42    | Li-ion  | 1     | 00D3EF2D8A |
|            |               | 8     | Li-ion  | 1     | FAB48926D9 |
| BYD        | DELL TMFYT8C  | 75.0  | Li-poly | 1     | 07A9851CD6 |
| Hewlett... | Primary       | 46    | Li-ion  | 1     | E5DA00D166 |
| Hewlett... | Primary       | 51    | Li-ion  | 1     | D9CA1DBE13 |
| Hewlett... | Primary       | 72    | Li-ion  | 1     | 121C592517 |
| Hewlett... | Primary       | 63    | Li-ion  | 1     | 74DD313167 |
| Hewlett... | Primary       | 81    | Li-ion  | 1     | A9509A2DE7 |
| LG A50     | A50           | 41.6  | Li-ion  | 1     | 5EA0111CED |
| LGC        | 00HW020       | 52.5  | Li-poly | 1     | 3C1A4327F5 |
| LGC        | 01AV432       | 51.0  | Li-poly | 1     | 0818236221 |
| LGC        | 01AV457       | 56.0  | Li-poly | 1     | 3AC5B5C4AD |
| Medion     |  lithium-ion  | 42.1  | Li-ion  | 1     | 11EC0315D7 |
| OEM        | BS1224E       | 72.8  |         | 1     | 6D2724E820 |
|            | PA3593U-1BRS  | 45.4  | Li-ion  | 1     | 7DA5C4A4FA |
| SIMPLO     | PABAS0241231  | 41.6  | Li-ion  | 1     | A295EE15E6 |
| SMP        | 01AV441       | 56    | Li-poly | 1     | 1D52B52B65 |
| SMP        | DELL 39DY56B  | 37.9  | Li-poly | 1     | FD6A48B8F8 |
| SMP        | DELL N18GG65  | 60.0  | Li-poly | 1     | 08EE9F4224 |
| SMP        | L17M3P61      | 36.0  | Li-poly | 1     | C3AA237F35 |

