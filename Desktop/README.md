Most popular CPU and RAM in Desktops
====================================

See more info in the [README](https://github.com/linuxhw/DMI).

Contents
--------

1. [ Devices in Desktops ](#devices)
   * [ Cpu ](#cpu)
   * [ Memory ](#memory)

Devices
-------

Count  — number of computers with this device installed,
Probe  — latest probe ID of this device.

### Cpu

| MFG        | Name                           | GHz  | Count | Probe      |
|------------|--------------------------------|------|-------|------------|
| Intel      | Core 2 Duo E8400               | 3.00 | 137   | D232DBDCE3 |
| AMD        | FX -6300 Six-Core              |      | 122   | 1F7F86ED9E |
| Intel      | Core i3-2120                   | 3.30 | 117   | 4BD42BC14C |
| Intel      | Core 2 Quad Q6600              | 2.40 | 109   | 53C6C139DA |
| Intel      | Core 2 Duo E7500               | 2.93 | 103   | 3134315807 |
| AMD        | FX-8350 Eight-Core             |      | 100   | FEAA959521 |
| Intel      | Core i3-2100                   | 3.10 | 98    | EB9D0FE3F5 |
| AMD        | Athlon II X2 250               |      | 97    | 07BA810409 |
| Intel      | Pentium Dual-Core E5300        | 2.60 | 97    | 0E497514B2 |
| Intel      | Core i3-3220                   | 3.30 | 88    | 7D1A3490AA |
| Intel      | Core i5-3470                   | 3.20 | 88    | 2703EE0766 |
| Intel      | Core i5-2400                   | 3.10 | 86    | 66A5EA0BE5 |
| Intel      | Core 2 Duo E6550               | 2.33 | 83    | 969A371A99 |
| AMD        | FX-8320 Eight-Core             |      | 78    | 6A60A724F9 |
| Intel      | Pentium 4                      | 3.00 | 75    | B8D44517A1 |
| Intel      | Core i7-3770K                  | 3.50 | 74    | FB18F180A5 |
| Intel      | Core i7-3770                   | 3.40 | 72    | 41F60D966A |
| Intel      | Pentium 4                      | 3.00 | 71    | 642FED58C5 |
| AMD        | FX -4300 Quad-Core             |      | 69    | C79641FC9B |
| Intel      | Core i5-4460                   | 3.20 | 69    | 91C1AFF11E |
| Intel      | Core 2 Duo E4500               | 2.20 | 65    | 1ACC3BBABB |
| Intel      | Pentium Dual-Core E5700        | 3.00 | 64    | AD36B40663 |
| Intel      | Pentium Dual E2180             | 2.00 | 59    | A0BFCB3B91 |
| Intel      | Core i5-2500                   | 3.30 | 59    | 3D9E77AE8A |
| AMD        | Phenom II X4 965               |      | 58    | F0FD8645E5 |
| Intel      | Pentium 4                      | 3.20 | 58    | 59817A0992 |
| Intel      | Core i7-2600                   | 3.40 | 57    | 55527427F9 |
| Intel      | Core i7-2600K                  | 3.40 | 57    | EC6555DA94 |
| Intel      | Core 2 Duo E8400               | 3.00 | 56    | 2893794AF3 |
| Intel      | Core 2 Duo E8500               | 3.16 | 55    | E1D8ADF3C9 |
| Intel      | Pentium Dual-Core E5400        | 2.70 | 55    | 1BDFB65A5B |
| Intel      | Core i5-2500K                  | 3.30 | 55    | 640C9F1AB7 |
| Intel      | Core i5-3570K                  | 3.40 | 55    | 0BFC90B6BA |
| AMD        | FX -4100 Quad-Core             |      | 54    | ABBA273D4F |
| AMD        | FX -6100 Six-Core              |      | 54    | 792BC1DD6F |
| AMD        | Athlon 64 X2 Dual Core 5200+   |      | 53    | F87C76BA83 |
| Intel      | Pentium Dual E2200             | 2.20 | 53    | D3B45DE297 |
| AMD        | Athlon 64 X2 Dual Core 6000+   |      | 51    | EE184BFE51 |
| AMD        | Phenom II X4 955               |      | 51    | 92145D937F |
| Intel      | Core 2 Duo E6750               | 2.66 | 51    | 48709E5844 |
| Intel      | Pentium Dual E2160             | 1.80 | 50    | FFC9B5ED4F |
| Intel      | Core 2 Duo E7200               | 2.53 | 50    | F8AE92B32F |
| Intel      | Core i5-3570                   | 3.40 | 50    | D3CBC50580 |
| AMD        | Athlon II X4 640               |      | 49    | 28A9991FCB |
| Intel      | Celeron E3400                  | 2.60 | 49    | B023115CE4 |
| Intel      | Core i3-6100                   | 3.70 | 49    | BD6DC70775 |
| Intel      | Pentium 4                      | 2.80 | 48    | D2C80DA520 |
| Intel      | Pentium D                      | 3.20 | 48    | C6129C8C14 |
| Intel      | Pentium G2020                  | 2.90 | 47    | 73CB5810A1 |
| AMD        | Athlon II X2 240               |      | 46    | 2DE6892C13 |
| Intel      | Celeron                        | 2.66 | 46    | 231DE8EA15 |
| Intel      | Celeron                        | 2.93 | 46    | 8219FBB532 |
| Intel      | Core 2 Quad Q8300              | 2.50 | 46    | BBFC5C83ED |
| Intel      | Core 2 Duo E7400               | 2.80 | 45    | 2FDD668590 |
| Intel      | Core i3-3240                   | 3.40 | 45    | F2FF6FB037 |
| Intel      | Core 2 Duo E4600               | 2.40 | 44    | 04AE7D50B4 |
| Intel      | Core i5-2320                   | 3.00 | 44    | 9FAB0B6F0B |
| Intel      | Core i7-4770                   | 3.40 | 44    | B687FAFEA4 |
| Intel      | Pentium G620                   | 2.60 | 43    | 8871866F65 |
| Intel      | Pentium G630                   | 2.70 | 43    | 11581C9025 |
| Intel      | Pentium Dual-Core E5200        | 2.50 | 42    | 758924E4F2 |
| Intel      | Core i5-3450                   | 3.10 | 42    | 9EF947B594 |
| Intel      | Pentium G3220                  | 3.00 | 42    | 3859A17AD1 |
| AMD        | Athlon 64 X2 Dual Core 4000+   |      | 41    | B08E58BFEB |
| Intel      | Pentium D                      | 2.66 | 41    | 7B78886CE4 |
| Intel      | Core i5 760                    | 2.80 | 41    | 85F57C764C |
| Intel      | Core i5-2300                   | 2.80 | 41    | E971779389 |
| AMD        | Athlon 64 X2 Dual Core 4200+   |      | 40    | 31EC690BE3 |
| Intel      | Core 2 6600                    | 2.40 | 40    | 7B106B9427 |
| Intel      | Core i5 750                    | 2.67 | 40    | 551193665F |
| Intel      | Core i3-4130                   | 3.40 | 40    | 02CBA6FDA0 |
| Intel      | Core i5-4570                   | 3.20 | 40    | 1E5997F695 |
| Intel      | Pentium D                      | 3.40 | 39    | 0C62C4C191 |
| Intel      | Core 2 Duo E7300               | 2.66 | 39    | 3991181C75 |
| Intel      | Core i7-4790                   | 3.60 | 39    | 2272E1FA1D |
| Intel      | Core i3 550                    | 3.20 | 38    | 12256732C2 |
| Intel      | Core i5-2310                   | 2.90 | 38    | A1E970227D |
| Intel      | Core i5-3330                   | 3.00 | 38    | 0A593D376A |
| Intel      | Core i3-7100                   | 3.90 | 37    | BD3CFBCE71 |
| Intel      | Core 2 Quad Q9300              | 2.50 | 37    | 36D3F979DC |
| Intel      | Celeron G530                   | 2.40 | 37    | B1C86BAC64 |
| Intel      | Pentium G2030                  | 3.00 | 37    | 154C64D953 |
| Intel      | Core i5-4440                   | 3.10 | 37    | 1DBE9C51F6 |
| Intel      | Pentium G4400                  | 3.30 | 37    | 0BF4CAF942 |
| AMD        | Athlon 64 X2 Dual Core 5000+   |      | 36    | B4B7FB1E21 |
| Intel      | Core i3 540                    | 3.07 | 36    | B4F81E84C8 |
| Intel      | Celeron G1840                  | 2.80 | 36    | 8AE6B6F651 |
| Intel      | Core i7-4770K                  | 3.50 | 35    | CC9738C393 |
| AMD        | Athlon 64 X2 Dual Core 4400+   |      | 34    | CD4ECC0B42 |
| Intel      | Celeron 430                    | 1.80 | 34    | 6154EA6AE1 |
| Intel      | Core i5-6500                   | 3.20 | 34    | E4A1AC8DFE |
| Intel      | Pentium 4                      | 3.20 | 33    | 5CED2505B9 |
| Intel      | Atom D525                      | 1.80 | 33    | 682BC26535 |
| Intel      | Core i3 530                    | 2.93 | 33    | 7D62E5BC34 |
| AMD        | Athlon 64 X2 Dual Core 4800+   |      | 32    | 9B11B923A2 |
| AMD        | Athlon II X2 245               |      | 32    | E3128E9139 |
| Intel      | Pentium G860                   | 3.00 | 32    | 4C163D8A71 |
| Intel      | Core i3-4170                   | 3.70 | 32    | 66D36BB608 |
| AMD        | Athlon II X2 215               |      | 31    | C9F702BE07 |
| AMD        | Athlon II X2 220               |      | 31    | E327FDD558 |
| AMD        | Athlon II X2 270               |      | 31    | CC22922655 |
| Intel      | Pentium Dual-Core E5200        | 2.50 | 31    | F6B9026258 |
| AMD        | Athlon 64 X2 Dual Core 5600+   |      | 30    | 5824A23FE2 |
| Intel      | Pentium 4                      | 3.00 | 30    | 03B6DE378D |
| Intel      | Pentium Dual-Core E6500        | 2.93 | 30    | 6DB0F0B43C |
| Intel      | Core i5-4670                   | 3.40 | 30    | F79161999A |
| Intel      | Core 2 Quad Q8400              | 2.66 | 29    | FAC5E9FEFA |
| Intel      | Pentium Dual-Core E6600        | 3.06 | 29    | 60A3A2F64B |
| Intel      | Core i7-4790K                  | 4.00 | 29    | 7552A8CB4C |
| Intel      | Core 2 4300                    | 1.80 | 28    | BF99369A01 |
| Intel      | Core i5-7400                   | 3.00 | 28    | 2048B1ECF6 |
| Intel      | Celeron E3300                  | 2.50 | 28    | BFC8CAB47C |
| Intel      | Core 2 Quad Q9400              | 2.66 | 28    | F640B8A0CB |
| AMD        | Athlon II X2 250               |      | 27    | 45F3041E55 |
| Intel      | Core i5-3550                   | 3.30 | 27    | 435E64F300 |
| Intel      | Core i5-4670K                  | 3.40 | 27    | 3166CD0BE4 |
| Intel      | Core i7-6700                   | 3.40 | 27    | DA78DC8E90 |
| AMD        | Athlon 64 X2 Dual Core 4200+   |      | 26    | 4F9FB84CF3 |
| AMD        | Athlon 64 X2 Dual Core 3800+   |      | 26    | 0478804BE2 |
| AMD        | Phenom II X6 1055T             |      | 26    | C44B707FC4 |
| AMD        | Athlon II X4 630               |      | 26    | 4950E6BB13 |
| AMD        | Athlon II X2 260               |      | 26    | 17C0C75B39 |
| Intel      | Core 2 Duo E6850               | 3.00 | 26    | A4A45B8A8B |
| Intel      | Core 2 6400                    | 2.13 | 26    | 9648FF0793 |
| Intel      | Pentium G850                   | 2.90 | 26    | F68209E673 |
| Intel      | Core i5-4430                   | 3.00 | 26    | 32D6CC8A86 |
| Intel      | Core i5-4690K                  | 3.50 | 26    | 19AD634C13 |
| AMD        | Athlon 64 3200+                |      | 25    | D753273D7B |
| AMD        | Athlon II X4 620               |      | 25    | 24F00A45BF |
| Intel      | Core 2 Quad Q8200              | 2.33 | 25    | DEB6214FDD |
| AMD        | Athlon II X3 450               |      | 24    | 4B5D7735D0 |
| AMD        | Athlon II X4 645               |      | 24    | A9ED28807E |
| AMD        | Ryzen 5 1600 Six-Core          |      | 24    | 03B0E6294F |
| Intel      | Core i3-4160                   | 3.60 | 24    | 9F42078526 |
| Intel      | Core i5-6400                   | 2.70 | 24    | 99614383EB |
| AMD        | Athlon II X3 440               |      | 23    | 7C80BF6FDA |
| AMD        | A8-7600 Radeon R7, 10 Compu... |      | 23    | 33CFE3D019 |
| AMD        | Ryzen 3 2200G with Radeon V... |      | 23    | 2B309F6855 |
| Intel      | Core 2 6300                    | 1.86 | 23    | 7038954CEF |
| Intel      | Pentium Dual-Core E6300        | 2.80 | 23    | 84C3697ACF |
| Intel      | Core 2 Duo E8500               | 3.16 | 23    | 3B5FB81B88 |
| Intel      | Celeron G1610                  | 2.60 | 23    | 2D5421E731 |
| Intel      | Core i3-4330                   | 3.50 | 23    | 8F8E9AFD24 |
| Intel      | Core i5-4690                   | 3.50 | 23    | 66882A7F7D |
| AMD        | Athlon 64 X2 Dual Core 5600+   |      | 22    | DAA890901E |
| AMD        | Phenom II X4 945               |      | 22    | A270C78534 |
| AMD        | A6-5400K APU with Radeon HD... |      | 22    | 77FDDE1684 |
| AMD        | Ryzen 7 1700 Eight-Core        |      | 22    | 880E3D4D52 |
| Intel      | Pentium 4                      | 2.40 | 22    | 6864E03577 |
| Intel      | Celeron E1400                  | 2.00 | 22    | 4DAC736908 |
| Intel      | Pentium G4560                  | 3.50 | 22    | 9B15CCEBAB |
| Intel      | Pentium G840                   | 2.80 | 22    | 8EE1E332AF |
| Intel      | Core i5-4590                   | 3.30 | 22    | 7BBCD6F17D |
| Intel      | Core i7-6700K                  | 4.00 | 22    | 89F4BB64D7 |
| AMD        | Phenom II X4 945               |      | 21    | FA67D28BA1 |
| AMD        | A10-5800K APU with Radeon H... |      | 21    | 88E3DD8AEA |
| AMD        | A4-5300 APU with Radeon HD ... |      | 21    | AEC91031D4 |
| AMD        | A4-6300 APU with Radeon HD ... |      | 21    | 8BCC9C4C78 |
| Intel      | Core 2 Quad Q9550              | 2.83 | 21    | 77480282EB |
| Intel      | Atom 330                       | 1.60 | 21    | 1CADF5EB87 |
| Intel      | Celeron J1800                  | 2.41 | 21    | 35CFA6018F |
| Intel      | Pentium G3420                  | 3.20 | 21    | 9770B67FA6 |
| AMD        | Athlon 64 X2 Dual Core 5400+   |      | 20    | B28773FBED |
| AMD        | Athlon II X3 445               |      | 20    | A1ADC281B3 |
| AMD        | FX -4350 Quad-Core             |      | 20    | E68A02C0A3 |
| AMD        | FX-8300 Eight-Core             |      | 20    | F79EDA31F9 |
| Intel      | Core 2 Duo E8600               | 3.33 | 20    | 7D6812488D |
| Intel      | Pentium Dual-Core E5500        | 2.80 | 20    | 1E185BA638 |
| Intel      | Core 2 Duo E8200               | 2.66 | 20    | 64F39F56BD |
| Intel      | Core i7 930                    | 2.80 | 20    | 7C64EA2E6D |
| Intel      | Core i3 540                    | 3.07 | 20    | DA83A24C25 |
| Intel      | Core i5 650                    | 3.20 | 20    | F58375E009 |
| Intel      | Core i3-4150                   | 3.50 | 20    | 097B4FFD71 |
| Intel      | Pentium G3260                  | 3.30 | 20    | C76B2FB327 |
| AMD        | Phenom II X2 555               |      | 19    | 08D3C8B273 |
| AMD        | Athlon II X4 635               |      | 19    | 583C276AD3 |
| AMD        | FX-8120 Eight-Core             |      | 19    | AAA77437D5 |
| Intel      | Pentium 4                      | 2.80 | 19    | 4BF2C616B1 |
| Intel      | Pentium Dual E2140             | 1.60 | 19    | 260C76AAF6 |
| Intel      | Core 2 6300                    | 1.86 | 19    | 09F65DEA65 |
| Intel      | Core i7-7700K                  | 4.20 | 19    | 1E9BB53944 |
| Intel      | Core 2 Quad Q8200              | 2.33 | 19    | 5F6E1A3B61 |
| Intel      | Celeron G3900                  | 2.80 | 19    | A3868738D5 |
| AMD        | Athlon X4 740 Quad Core        |      | 18    | C7B45B917D |
| AMD        | A4-4000 APU with Radeon HD ... |      | 18    | B10EB3E198 |
| AMD        | FX-8320E Eight-Core            |      | 18    | 596AFEF750 |
| AMD        | Athlon X4 840 Quad Core        |      | 18    | 88E51DFA85 |
| Intel      | Celeron                        | 2.60 | 18    | AE7CD5DF2C |
| Intel      | Pentium 4                      | 3.00 | 18    | CE41D485F3 |
| Intel      | Pentium Dual-Core E5800        | 3.20 | 18    | E822FDB214 |
| Intel      | Xeon E5450                     | 3.00 | 18    | 1039518044 |
| Intel      | Core i7 860                    | 2.80 | 18    | C5B8862A26 |
| Intel      | Core i5 650                    | 3.20 | 18    | 05DC50AA11 |
| Intel      | Celeron G1620                  | 2.70 | 18    | 07CABFD5DD |
| Intel      | Celeron G1820                  | 2.70 | 18    | 43487CB040 |
| AMD        | Athlon 64 X2 Dual Core 3600+   |      | 17    | 4E526BB85F |
| AMD        | A4-3400 APU with Radeon HD ... |      | 17    | D969A32092 |
| AMD        | A8-6600K APU with Radeon HD... |      | 17    | 426AE68B93 |
| AMD        | Athlon X4 860K Quad Core       |      | 17    | D6529B50FB |
| AMD        | Ryzen 5 2400G with Radeon V... |      | 17    | 76E2078928 |
| Intel      | Celeron D                      | 3.06 | 17    | BD61806194 |
| Intel      | Core 2 4400                    | 2.00 | 17    | BBB6C77D49 |
| Intel      | Core 2 Duo E7600               | 3.06 | 17    | E7AD4E06B0 |
| Intel      | Pentium Dual-Core E6700        | 3.20 | 17    | 71A2CD183B |
| Intel      | Celeron 1037U                  | 1.80 | 17    | 90EAD0E5EE |
| AMD        | Phenom II X6 1090T             |      | 16    | F164DA5974 |
| AMD        | Athlon II X3 425               |      | 16    | DD7F054971 |
| AMD        | Athlon II X2 245               |      | 16    | BCD563149D |
| AMD        | FX -4130 Quad-Core             |      | 16    | B2DB9E2035 |
| AMD        | FX-8150 Eight-Core             |      | 16    | CFDCEB1356 |
| AMD        | A10-6800K APU with Radeon H... |      | 16    | CEDA0A30C3 |
| Intel      | Genuine 2140                   | 1.60 | 16    | CA59692108 |
| Intel      | Core 2 Quad Q9450              | 2.66 | 16    | C8473D0EF0 |
| Intel      | Core i7-3820                   | 3.60 | 16    | BD6FF7AA0F |
| Intel      | Celeron J1900                  | 1.99 | 16    | 3450D4540C |
| Intel      | Pentium G3240                  | 3.10 | 16    | D73E7CDAF7 |
| Intel      | Core i5-6600K                  | 3.50 | 16    | E854740490 |
| AMD        | Athlon 64 X2 Dual Core 4400+   |      | 15    | BB9A824482 |
| AMD        | Athlon 64 X2 Dual Core 3800+   |      | 15    | 784F37710B |
| AMD        | Phenom II X6 1075T             |      | 15    | A4119B2AD7 |
| AMD        | Athlon II X3 455               |      | 15    | 82658003D4 |
| AMD        | Sempron 140                    |      | 15    | 9643FFAA28 |
| AMD        | FX -6350 Six-Core              |      | 15    | 5711BD1130 |
| AMD        | A10-7850K Radeon R7, 12 Com... |      | 15    | E1D561ABB4 |
| AMD        | Ryzen 5 2600 Six-Core          |      | 15    | BF6F02A425 |
| Intel      | Core 2 Duo E4700               | 2.60 | 15    | 982ACF395B |
| Intel      | Pentium Dual E2220             | 2.40 | 15    | 3287F20A2A |
| Intel      | Core 2 6320                    | 1.86 | 15    | ADB4F9193B |
| Intel      | Core 2 6420                    | 2.13 | 15    | 943832F4C7 |
| Intel      | Core 2 Quad                    | 2.40 | 15    | 574DA789B0 |
| Intel      | Core i5-7500                   | 3.40 | 15    | 239A2E1678 |
| Intel      | Core i7 950                    | 3.07 | 15    | A265BAD98F |
| Intel      | Celeron G540                   | 2.50 | 15    | 0D28868F69 |
| Intel      | Core i3-3210                   | 3.20 | 15    | C2DCE3184C |
| AMD        | Phenom 8450 Triple-Core        |      | 14    | 3F9F5FB840 |
| AMD        | Phenom 9550 Quad-Core          |      | 14    | 56210ACC56 |
| AMD        | Athlon II X2 240               |      | 14    | 8F8DBB4243 |
| AMD        | Athlon II X2 255               |      | 14    | C8CDE63BC8 |
| AMD        | A8-3870 APU with Radeon HD ... |      | 14    | 961DE73328 |
| AMD        | E-350                          |      | 14    | 86B54A46B1 |
| AMD        | A4-7300 APU with Radeon HD ... |      | 14    | E232F2DE61 |
| Intel      | Celeron E1200                  | 1.60 | 14    | 9D3DBD169B |
| Intel      | Genuine 2160                   | 1.80 | 14    | 1E807288A2 |
| Intel      | Core i7-7700                   | 3.60 | 14    | 0FE2508B1C |
| Intel      | Celeron E3200                  | 2.40 | 14    | BA2D406643 |
| Intel      | Xeon E5450                     | 3.00 | 14    | A2768338C0 |
| Intel      | Atom 230                       | 1.60 | 14    | E8574A6711 |
| Intel      | Pentium G2120                  | 3.10 | 14    | A67F2E1C4D |
| Intel      | Pentium G2130                  | 3.20 | 14    | D90CC4CBE3 |
| AMD        | Athlon 64 X2 Dual Core 6000+   |      | 13    | 72886E4425 |

### Memory

| MFG        | Name               | Size     | Type | MT/s | Count | Probe      |
|------------|--------------------|----------|------|------|-------|------------|
|            | Module DIMM        | 2048 MB  | DDR2 | 800  | 429   | 2FDD668590 |
|            | Module SDRAM       | 2048 MB  |      |      | 400   | F8AE92B32F |
|            | Module             | 2048 MB  |      | 800  | 371   | F6B9026258 |
|            | Module SDRAM       | 1024 MB  |      |      | 368   | 34021FD6BD |
|            | Module             | 4096 MB  |      | 1333 | 332   | A9ED28807E |
|            | Module             | 2048 MB  |      | 1333 | 320   | 551193665F |
|            | Module DIMM        | 1024 MB  | DDR2 | 800  | 290   | 48709E5844 |
|            | Module             | 1024 MB  |      | 800  | 217   | F6B9026258 |
|            | Module SODIMM      | 1024 MB  | DDR2 | 667  | 212   | EE184BFE51 |
|            | Module DIMM        | 2048 MB  | DDR2 | 667  | 173   | EE184BFE51 |
|            | Module SODIMM      | 4096 MB  | DDR3 | 1333 | 154   | 6A60A724F9 |
|            | Module             | 2048 MB  |      | 667  | 148   | E1D8ADF3C9 |
|            | Module             | 1024 MB  |      | 667  | 143   | 6DB0F0B43C |
|            | Module             | 2048 MB  |      | 400  | 124   | 982ACF395B |
|            | Module SDRAM       | 512 MB   |      |      | 122   | 642FED58C5 |
|            | Module             | 4096 MB  |      | 1600 | 121   | C79641FC9B |
|            | Module             | 1024 MB  |      |      | 115   | 38155B66BB |
| Kingston   | KHX1600C9D3/4GX... | 4096 MB  | DDR3 | 2400 | 106   | 1DBE9C51F6 |
|            | Module SODIMM      | 2048 MB  | DDR3 | 1333 | 92    | 961DE73328 |
|            | Module DIMM        | 4096 MB  | DDR3 | 1600 | 87    | B801DD3F7D |
|            | Module             | 1024 MB  | DDR2 |      | 86    | CD4ECC0B42 |
|            | Module DDR         | 2048 MB  |      | 1333 | 85    | 966D463C0C |
|            | Module             | 1024 MB  | DDR2 | 333  | 84    | F87C76BA83 |
|            | Module             | 4096 MB  |      | 400  | 84    | 666B4349AD |
|            | Module             | 2048 MB  |      | 1066 | 81    | A2DBF52E17 |
| Kingston   | KHX1600C10D3/8G... | 8192 MB  | DDR3 | 1600 | 71    | 0C7ACF2C99 |
|            | Module             | 2048 MB  | DDR2 |      | 68    | FFC9B5ED4F |
|            | Module             | 2048 MB  |      |      | 67    | 1293120120 |
|            | Module             | 2048 MB  | DDR2 | 333  | 67    | 7D6812488D |
| Kingston   | 99U5471-012.A00... | 4096 MB  | DDR3 | 1600 | 66    | 724F1F5F22 |
|            | Module             | 512 MB   |      |      | 65    | 2E9805CB56 |
|            | Module             | 1024 MB  |      | 400  | 59    | BD4727E558 |
|            | Module SDRAM       | 4096 MB  |      |      | 56    | 5F6E1A3B61 |
| Kingston   | 99U5584-005.A00... | 4096 MB  | DDR3 | 1600 | 53    | C0B50F9AE1 |
|            | Module DDR         | 2048 MB  |      | 800  | 52    | C92DC5D0CF |
|            | Module DIMM        | 512 MB   | DDR2 | 667  | 51    | 3DD6E989DE |
|            | Module             | 2048 MB  |      | 1600 | 49    | 2F8132A2A1 |
|            | Module             | 2048 MB  | DDR2 | 400  | 48    | C44B707FC4 |
| Kingston   | 99U5471-020.A00... | 4096 MB  | DDR3 | 1600 | 47    | 88E3DD8AEA |
|            | Module             | 4096 MB  |      | 667  | 45    | F0B3D3251C |
|            | Module             | 4096 MB  |      | 1066 | 44    | 7C64EA2E6D |
|            | Module             | 512 MB   |      | 667  | 39    | 6DB0F0B43C |
|            | Module SODIMM      | 8192 MB  | DDR3 | 1600 | 38    | 8C6609B568 |
| Samsung    | M378B5273DH0-CH... | 4096 MB  | DDR3 | 2133 | 38    | C2DCE3184C |
| Kingston   | KHX1866C10D3/8G... | 8192 MB  |      | 2133 | 37    | E969E52D33 |
|            | Module             | 8192 MB  |      | 1333 | 37    | A9ED28807E |
| Samsung    | M378B5773CH0-CH... | 2048 MB  | DDR3 | 1867 | 37    | 66A5EA0BE5 |
| SK Hynix   | HMT351U6CFR8C-H... | 4096 MB  | DDR3 | 1600 | 36    | 8798B74A8F |
| Samsung    | M378B5673FH0-CH... | 2048 MB  | DDR3 | 1600 | 36    | C8CDE63BC8 |
| Kingston   | KHX1866C10D3/4G... | 4096 MB  | DDR3 | 1866 | 35    | 92145D937F |
|            | Module SDRAM       | 2048 MB  |      | 800  | 35    | 72A18A259D |
|            | Module SDRAM       | 256 MB   |      |      | 35    | 231DE8EA15 |
|            | Module             | 4096 MB  |      | 800  | 35    | E411ED27AE |
| Samsung    | M378B5773DH0-CH... | 2048 MB  | DDR3 | 1333 | 35    | 9F42078526 |
|            | Module DIMM DDR    | 1024 MB  |      | 667  | 34    | A2E86EFF2D |
|            | Module DDR         | 1024 MB  |      | 800  | 34    | CE5CCA6404 |
|            | Module             | 1024 MB  | DDR2 | 266  | 34    | 0478804BE2 |
|            | Module SDRAM       | 1024 MB  |      | 667  | 34    | 1E807288A2 |
|            | Module SODIMM      | 1024 MB  | DDR2 | 533  | 33    | 4E526BB85F |
|            | Module DDR         | 4096 MB  |      | 1333 | 33    | 05DC50AA11 |
| Kingston   | KHX1600C10D3/8G... | 8192 MB  | DDR3 | 1600 | 32    | 680A34868A |
| Kingston   | 99U5474-028.A00... | 4096 MB  | DDR3 | 1333 | 31    | C76B2FB327 |
|            | Module SDRAM       | 1024 MB  |      | 533  | 31    | A4A45B8A8B |
|            | Module             | 4096 MB  |      |      | 31    | 16ECB5A13F |
|            | Module DIMM        | 8192 MB  | DDR3 | 1333 | 31    | 7CF6D970EC |
| Samsung    | M378B5273CH0-CH... | 4096 MB  | DDR3 | 1867 | 31    | 2703EE0766 |
| Kingston   | KHX1600C10D3/4G... | 4096 MB  | DDR3 | 1600 | 30    | 66D36BB608 |
|            | Module DIMM DDR    | 2048 MB  |      | 667  | 30    | A2E86EFF2D |
|            | Module             | 1024 MB  |      | 1333 | 29    | EEEE376F38 |
|            | Module DDR         | 1024 MB  |      | 400  | 29    | 27FC10C18A |
|            | Module DIMM        | 2048 MB  | DDR2 | 533  | 28    | 7C6281F09B |
| Crucial    | CT51264BA160B.C... | 4096 MB  | DDR3 | 1600 | 27    | 762D77E60F |
|            | Module             | 1024 MB  |      | 66   | 26    | 3F25A03C59 |
|            | Module             | 2048 MB  | DDR2 | 266  | 26    | BD61806194 |
| Kingston   | KHX1866C9D3/4GX... | 4096 MB  | DDR3 | 1867 | 25    | 56C7F5C529 |
|            | Module DDR         | 1024 MB  |      |      | 25    | 04762D9B42 |
|            | Module SDRAM       | 2048 MB  |      | 533  | 25    | 9643FFAA28 |
| Corsair    | CMX8GX3M2A1600C... | 4096 MB  | DDR3 | 1600 | 24    | EC6555DA94 |
|            | Module             | 256 MB   |      |      | 24    | 94E705D4A2 |
|            | Module DIMM        | 512 MB   | DDR2 |      | 23    | 9DF0CEE262 |
|            | Module             | 8192 MB  |      | 1600 | 23    | 792BC1DD6F |
| Corsair    | CMZ8GX3M2A1600C... | 4096 MB  | DDR3 | 1600 | 22    | EC6555DA94 |
| SK Hynix   | HMT351U6CFR8C-P... | 4096 MB  | DDR3 | 1800 | 22    | F0D35357D4 |
|            | Module             | 4096 MB  | DDR3 | 800  | 22    | ACB1DA03CD |
|            | Module             | 1024 MB  | DDR2 | 400  | 21    | C4135693FC |
|            | Module             | 512 MB   | DDR2 | 333  | 21    | 753ED60051 |
|            | Module SDRAM       | 1024 MB  |      | 800  | 20    | 1BC2451FA9 |
| Kingston   | 9905471-001.A01... | 2048 MB  | DDR3 | 1600 | 19    | F2560038AF |
| Kingston   | KHX2133C11D3/4G... | 4096 MB  | DDR3 | 2133 | 19    | 927A46266A |
| Kingston   | KHX2400C11D3/4G... | 4096 MB  | DDR3 | 2400 | 19    | 0A4CDB301F |
|            | Module SDRAM       | 2048 MB  |      | 667  | 19    | 1E807288A2 |
|            | Module             | 512 MB   |      | 400  | 19    | 119DCF1A1D |
|            | Module             | 512 MB   |      | 66   | 19    | 5D3AF9A12C |
| Kingston   | 9905403-439.A00... | 4096 MB  | DDR3 | 1600 | 18    | 2CA17B3AA6 |
| Kingston   | 99U5471-054.A00... | 8192 MB  | DDR3 | 1600 | 18    | DEE955AF65 |
|            | Module DIMM        | 2048 MB  | DDR3 | 1067 | 18    | DDE51ECD79 |
|            | Module             | 2048 MB  | DDR3 | 800  | 18    | AA7248927B |
|            | Module             | 512 MB   |      | 533  | 18    | 626C138C66 |
| Samsung    | M378B5173CB0-CK... | 4096 MB  | DDR3 | 2000 | 18    | 775809ECAF |
| Samsung    | M378B5173DB0-CK... | 4096 MB  | DDR3 | 1600 | 18    | 02CBA6FDA0 |
| Corsair    | CMV4GX3M1A1333C... | 4096 MB  | DDR3 | 1600 | 17    | 46E78A6EC1 |
| Kingston   | 99U5474-026.A00... | 4096 MB  | DDR3 | 1333 | 17    | 64BA0F1E35 |
| Kingston   | KHX2133C14D4/4G... | 4096 MB  | DDR4 | 2133 | 17    | 291EF2F99E |
|            | Module DDR         | 512 MB   |      | 333  | 17    | D753273D7B |
|            | Module DDR         | 512 MB   |      | 400  | 17    | B7CEC1644D |
| Crucial    | CT51264BA160B DIMM | 4096 MB  | DDR3 | 1600 | 16    | AB45A7DD05 |
| Kingston   | 99U5471-037.A00... | 8192 MB  | DDR3 | 1600 | 16    | F79161999A |
| Kingston   | KHX1600C10D3/ DIMM | 8192 MB  | DDR3 | 1600 | 16    | 0533339E4E |
| Kingston   | Module DIMM        | 2048 MB  | DDR2 | 800  | 16    | 758924E4F2 |
| Patriot    | PSD34G13332 DIMM   | 4096 MB  | DDR3 | 1333 | 16    | 7E70F0DCAB |
|            | Module DIMM        | 2048 MB  | DDR3 | 1600 | 16    | F3C86A9592 |
|            | Module SDRAM       | 2048 MB  |      | 1066 | 16    | 4C4FE1538E |
| GOODRAM    | GR1333D364L9/4G... | 4096 MB  | DDR3 | 1600 | 15    | 5626FAABD8 |
| Kingston   | 9905471-011.A00... | 4096 MB  | DDR3 | 1600 | 15    | 9FAB0B6F0B |
| Kingston   | 99U5474-016.A00... | 4096 MB  |      | 1333 | 15    | A60168346F |
| Kingston   | 99U5584-003.A00... | 4096 MB  | DDR3 | 1600 | 15    | 98CB227CB5 |
| Kingston   | 99U5584-010.A00... | 4096 MB  | DDR3 | 1600 | 15    | 40DDA90CBB |
| Kingston   | KHX2400C15/8G DIMM | 8192 MB  | DDR4 | 2400 | 15    | 936BEEBE68 |
|            | Module             | 1024 MB  |      | 333  | 15    | 6AC982730F |
|            | Module SODIMM      | 4096 MB  | DDR3 | 1067 | 15    | 5DE102EE2E |
|            | Module             | 512 MB   |      | 800  | 15    | 133D4E7388 |
| Samsung    | M3 78T2863QZS-C... | 1024 MB  | DDR2 | 800  | 15    | 3998C99464 |
| Samsung    | M378B5173QH0-CK... | 4096 MB  | DDR3 | 1866 | 15    | E169B6B458 |
| Samsung    | M378B5673GB0-CH... | 2048 MB  | DDR3 | 1333 | 15    | 6DC8CCD0F6 |
| Crucial    | BLS8G3D1609DS1S... | 8192 MB  | DDR3 | 1600 | 14    | FB18F180A5 |
| Crucial    | CT51264BA160BJ.... | 4096 MB  | DDR3 | 1600 | 14    | C9DF4AAFD7 |
| SK Hynix   | HMT325U6BFR8C-H... | 2048 MB  | DDR3 | 1333 | 14    | E971779389 |
| Kingston   | 9905474-012.A00... | 2048 MB  | DDR3 | 1333 | 14    | 7F0BF9C65F |
| Kingston   | 9905584-015.A00... | 4096 MB  | DDR3 | 1600 | 14    | 428E5A184D |
| Kingston   | 99P5474-013.A00... | 4096 MB  | DDR3 | 1600 | 14    | 900BC7E93C |
| Kingston   | 99U5471-040.A00... | 8192 MB  | DDR3 | 1333 | 14    | 435E64F300 |
| Kingston   | KHX1600C9D3/2GX... | 2048 MB  | DDR3 | 1600 | 14    | 6BD310EB97 |
|            | Module             | 2048 MB  |      | 533  | 14    | 8AB31FE48E |
|            | Module             | 4096 MB  | DDR2 | 800  | 14    | 682BC26535 |
|            | Module DIMM        | 512 MB   | DDR2 | 533  | 14    | 7C6281F09B |
| Samsung    | M3 78T5663QZ3-C... | 2048 MB  | DDR2 | 800  | 14    | 6A6E981372 |
| Samsung    | M378B5273CH0-CK... | 4096 MB  | DDR3 | 2000 | 14    | 9B14E30C35 |
| Corsair    | CMZ4GX3M1A1600C... | 4096 MB  | DDR3 | 1600 | 13    | A1DEF515BF |
| Crucial    | CT51264BD160B.C... | 4096 MB  | DDR3 | 1600 | 13    | 4D1EE88EC3 |
| Kingston   | 99U5471-001.A00... | 2048 MB  | DDR3 | 1333 | 13    | F2560038AF |
| Kingston   | 99U5471-002.A01... | 2048 MB  | DDR3 | 1333 | 13    | 8F74791E46 |
| Kingston   | 99U5471-052.A00... | 8192 MB  | DDR3 | 1333 | 13    | A2785304CB |
| Kingston   | 99U5474-010.A00... | 2048 MB  | DDR3 | 1333 | 13    | 2821399C59 |
| Kingston   | KHX1866C10D3/ DIMM | 4096 MB  | DDR3 | 1866 | 13    | 5711BD1130 |
|            | Module DDR         | 512 MB   |      |      | 13    | 03602DBEA6 |
|            | Module             | 8192 MB  |      | 667  | 13    | 8B977245CF |
| Samsung    | M378B5673EH1-CH... | 2048 MB  | DDR3 | 1333 | 13    | 7A06A01CC2 |
| SK Hynix   | HYMP112U64CP8-S... | 1024 MB  | DDR2 | 800  | 13    | 1A3B0634EB |
| Corsair    | CMX4GX3M1A1600C... | 4096 MB  | DDR3 | 1333 | 12    | 416F646796 |
| Corsair    | CMX8GX3M2A1333C... | 4096 MB  | DDR3 | 1333 | 12    | 9EF947B594 |
| SK Hynix   | HMT325U6CFR8C-H... | 2048 MB  | DDR3 | 1600 | 12    | 6A06B236F4 |
| Kingston   | 99U5595-002.A00... | 2048 MB  | DDR3 | 1400 | 12    | 6169DA7CBD |
| Kingston   | KHX2133C11D3/8G... | 8192 MB  | DDR3 | 2133 | 12    | 7B99FD4C95 |
| Kingston   | KHX2400C11D3/8G... | 8192 MB  | DDR3 | 2400 | 12    | DBEF73BCED |
| Kingston   | Module DIMM        | 1024 MB  | DDR2 | 667  | 12    | 7A6835908D |
| Micron     | 8JTF25664AZ-1G4... | 2048 MB  | DDR3 | 1333 | 12    | 35B986C185 |
|            | Module             | 512 MB   |      | 333  | 12    | 6AC982730F |
| Silicon... | DBLT4GN568S DIMM   | 4096 MB  | DDR3 | 1333 | 12    | 86080E5FB7 |
| Team       | Elite-1333 DIMM    | 4096 MB  | DDR3 | 1333 | 12    | 23ACB95370 |
| Corsair    | CMK16GX4M2B3000... | 8192 MB  | DDR4 | 2934 | 11    | A588768A6C |
| GOODRAM    | GR1333D364L9/2G... | 2048 MB  | DDR3 | 1333 | 11    | 20046D5D05 |
| SK Hynix   | HMT351U6EFR8C-P... | 4096 MB  | DDR3 | 1600 | 11    | CBA5D3466F |
| SK Hynix   | HMT451U6BFR8A-P... | 4096 MB  | DDR3 | 1600 | 11    | 9F42078526 |
| Kingston   | 2G-UDIMM DIMM DDR  | 2048 MB  |      | 800  | 11    | 53C6C139DA |
| Kingston   | 9905471-017.A00... | 4096 MB  | DDR3 | 1333 | 11    | 8AE6B6F651 |
| Kingston   | 9905678-024.A00... | 4096 MB  | DDR4 | 2133 | 11    | 7B1F69214A |
| Kingston   | ACR256X64D3U133... | 2048 MB  | DDR3 | 1333 | 11    | 49D2D34EB5 |
| Kingston   | KHX1600C9D3/8GX... | 8192 MB  | DDR3 | 1600 | 11    | D37E7AB88A |
| Kingston   | Module DIMM        | 2048 MB  | DDR2 | 667  | 11    | 53C6C139DA |
| Micron     | 16JTF51264AZ-1G... | 4096 MB  | DDR3 | 1333 | 11    | 5E8B8FCDBD |
| Micron     | ITC DIMM           | 4096 MB  | DDR3 | 1648 | 11    | A67F2E1C4D |
|            | Module DDR         | 1024 MB  |      | 1333 | 11    | 5D26A5028D |
|            | Module DRAM        | 1024 MB  |      |      | 11    | 940D3D20F9 |
|            | Module SDRAM       | 2048 MB  |      | 1333 | 11    | B4F81E84C8 |
|            | Module             | 4096 MB  | DDR3 | 1066 | 11    | B7DB1F6D08 |
|            | Module DIMM        | 4096 MB  | DDR3 | 667  | 11    | F6DE22FD19 |
| Samsung    | M378B1G73QH0-CK... | 8192 MB  | DDR3 | 1600 | 11    | D5AF14209B |
| Samsung    | M378B5173EB0-CK... | 4096 MB  |      | 1600 | 11    | D5C119CB28 |
| Silicon... | DCLT4GN128S DIMM   | 4096 MB  | DDR3 | 1600 | 11    | E7EE741AB9 |
| Corsair    | CML8GX3M2A1600C... | 4096 MB  | DDR3 | 1867 | 10    | 0308FCA137 |
| Corsair    | CMX4GX3M1A1333C... | 4096 MB  | DDR3 | 1333 | 10    | 5C758FAF74 |
| Elpida     | EBJ21UE8BDF0-DJ... | 2048 MB  | DDR3 | 1333 | 10    | CBBECB86EA |
| GeIL       | CL11-11-11 D3-1... | 8192 MB  | DDR3 | 1600 | 10    | 8CA925B490 |
| SK Hynix   | HMT451U6AFR8C-P... | 4096 MB  | DDR3 | 1600 | 10    | 2272E1FA1D |
| SK Hynix   | HMT325U6BFR8C-H... | 2048 MB  |      | 1333 | 10    | 80464E3EF6 |
| Kingston   | 9905403-478.A00... | 4096 MB  | DDR3 | 1867 | 10    | AEC80C497F |
| Kingston   | 99P5471-004.A01... | 4096 MB  | DDR3 | 1333 | 10    | 88E3DD8AEA |
| Kingston   | 99U5474-038.A00... | 4096 MB  | DDR3 | 1333 | 10    | 369FCFD593 |
| Kingston   | 99U5595-003.A00... | 2048 MB  | DDR3 | 1600 | 10    | 7E70F0DCAB |
| Kingston   | KHX2133C14/8G DIMM | 8192 MB  | DDR4 | 2133 | 10    | 4AC20149F9 |
| Kingston   | KHX2400C15D4/8G... | 8192 MB  |      | 2400 | 10    | 70016620CE |
| Nanya      | NT2GT64U8HD0BY-... | 2048 MB  | DDR2 | 800  | 10    | B4B7FB1E21 |
|            | Module DDR         | 1024 MB  |      | 333  | 10    | D753273D7B |
|            | Module DIMM        | 2048 MB  | DDR3 | 1066 | 10    | D809F01B15 |
|            | Module             | 512 MB   | DDR2 | 266  | 10    | BD61806194 |
|            | Module DRAM        | 512 MB   |      |      | 10    | E88218E9C4 |
| Samsung    | M378A5143DB0-CP... | 4096 MB  | DDR4 | 2400 | 10    | D00463167E |
| Samsung    | M378B5173BH0-CH... | 4096 MB  | DDR3 | 1867 | 10    | 2703EE0766 |
| Samsung    | M378B5273DH0-CK... | 4096 MB  | DDR3 | 2200 | 10    | 6ED6000D2E |
| Silicon... | DBLT2GN568S DIMM   | 2048 MB  | DDR3 | 1333 | 10    | F68209E673 |
| Corsair    | CMZ8GX3M2A1866C... | 4096 MB  | DDR3 | 1600 | 9     | 5595625922 |
| Crucial    | CT102464BA160B.... | 8192 MB  | DDR3 | 1600 | 9     | 640F0195DD |
| Crucial    | CT25664BA160B.C... | 2048 MB  | DDR3 | 1600 | 9     | C5B500154D |
| SK Hynix   | DMT451E6AFR8C-P... | 4096 MB  | DDR3 | 1600 | 9     | CFFA4C6577 |
| SK Hynix   | HMT325U6CFR8C-P... | 2048 MB  | DDR3 | 1600 | 9     | 112C1293E1 |
| SK Hynix   | HMT451U6MFR8C-P... | 4096 MB  | DDR3 | 1600 | 9     | 1CAC6CF78B |
| Kingston   | 9905584-017.A00... | 4096 MB  | DDR3 | 1600 | 9     | 68C1BC8BA6 |
| Kingston   | 99U5474-015.A00... | 2048 MB  | DDR3 | 1600 | 9     | F72A33F2BE |
| Kingston   | KHX3000C15D4/8G... | 8192 MB  | DDR4 | 3000 | 9     | 69F2264D39 |
| Kingston   | Module DIMM        | 1024 MB  | DDR2 | 800  | 9     | 9B671B15DD |
|            | Module             | 1024 MB  |      | 533  | 9     | B8ECD9CD1E |
|            | Module             | 256 MB   |      | 400  | 9     | BFA8B08B4A |
|            | Module DIMM        | 512 MB   | DDR2 | 800  | 9     | E327FDD558 |
| Samsung    | M378A1G43DB0-CP... | 8192 MB  |      | 2133 | 9     | B724162662 |
| Samsung    | M378B2873EH1-CH... | 1024 MB  | DDR3 | 1334 | 9     | 44FFC77631 |
| Samsung    | M378B2873FHS-CH... | 1024 MB  | DDR3 | 1333 | 9     | C8CDE63BC8 |
| Samsung    | M471B5173QH0-YK... | 4096 MB  | DDR3 | 1600 | 9     | 00FCD6CB57 |
| SK Hynix   | HYMP125U64CP8-S... | 2048 MB  | DDR2 | 800  | 9     | 1A3B0634EB |
| Team       | Elite-1600 DIMM    | 4096 MB  | DDR3 | 1600 | 9     | 19146B787B |
| Corsair    | CMK16GX4M2B3200... | 8192 MB  | DDR4 | 3200 | 8     | 4D739ABCDF |
| Corsair    | CMX4GX3M2A1600C... | 2048 MB  |      | 1600 | 8     | A4119B2AD7 |
| Crucial    | BLS4G3D1609DS1S... | 4096 MB  | DDR3 | 1600 | 8     | D6529B50FB |
| G.Skill    | F4-3200C16-8GVK... | 8192 MB  | DDR4 | 3200 | 8     | BF6F02A425 |
| Kingmax    | FLFE85F-C8KM9 DIMM | 2048 MB  | DDR3 | 1333 | 8     | 491DB7C6BD |
| Kingmax    | FLFF65F-C8KL9 DIMM | 4096 MB  | DDR3 | 1333 | 8     | D3017358B8 |
| Kingston   | 9905471-001.A00... | 2048 MB  | DDR3 | 1333 | 8     | 0DB5CC39B3 |
| Kingston   | 9905584-032.A00... | 4096 MB  | DDR3 | 1600 | 8     | 481B0A620E |
| Kingston   | 99U5471-002.A00... | 2048 MB  |      | 1333 | 8     | 4B8FD34544 |
| Micron     | 8JTF51264AZ-1G6... | 4096 MB  | DDR3 | 1600 | 8     | BBCA7D7D38 |
|            | Module DDR         | 1024 MB  |      | 200  | 8     | CE41D485F3 |
|            | Module             | 256 MB   |      | 333  | 8     | 4B6A3DE9F1 |
|            | Module DDR         | 256 MB   |      |      | 8     | 948D2E214B |
|            | Module DDR         | 256 MB   |      | 400  | 8     | 364473179A |
|            | Module             | 4096 MB  |      | 533  | 8     | 2DE6892C13 |
|            | Module SDRAM       | 4096 MB  |      | 1066 | 8     | 2E9A652851 |
| Samsung    | M3 78T5663EH3-C... | 2048 MB  | DDR2 | 2048 | 8     | 09F65DEA65 |
| Samsung    | M378A5244CB0-CR... | 4096 MB  | DDR4 | 2400 | 8     | 2048B1ECF6 |
| SK Hynix   | HYMP512U64CP8-Y... | 1024 MB  |      | 667  | 8     | 09F65DEA65 |
| Transcend  | JM1333KLN-2G DIMM  | 2048 MB  | DDR3 | 1333 | 8     | B52429650C |
| Transcend  | JM1333KLN-4G DIMM  | 4096 MB  | DDR3 | 1333 | 8     | D0BA8A3715 |
| Corsair    | CMZ16GX3M4A1600... | 4096 MB  | DDR3 | 1600 | 7     | 41F60D966A |
| Crucial    | CT4G4DFS8213.C8... | 4096 MB  | DDR4 | 2667 | 7     | 907EB904C3 |
| Crucial    | CT51264BA1339.C... | 4096 MB  | DDR3 | 1333 | 7     | 66F43E92ED |
| Crucial    | CT51264BA160BJ.... | 4096 MB  | DDR3 | 1600 | 7     | 762D77E60F |
| Crucial    | CT51264BA160BJ.... | 4096 MB  | DDR3 | 1600 | 7     | 63D9297F05 |
| Crucial    | ST51264BA1339.1... | 4096 MB  | DDR3 | 1333 | 7     | B2A08DC2BE |
| SK Hynix   | HMT351U6BFR8C-H... | 4096 MB  | DDR3 | 1333 | 7     | D5A5261203 |
| SK Hynix   | HMT325U6CFR8C-H... | 2048 MB  | DDR3 | 1333 | 7     | 80464E3EF6 |
| Ketech     | Module DIMM        | 4096 MB  | DDR3 | 1333 | 7     | 1CAB2B881B |
| Kingston   | 9905403-198.A00... | 4096 MB  | DDR3 | 1600 | 7     | BA845B8712 |

