Most popular CPU and RAM in All In Ones
=======================================

See more info in the [README](https://github.com/linuxhw/DMI).

Contents
--------

1. [ Devices in All In Ones ](#devices)
   * [ Cpu ](#cpu)
   * [ Memory ](#memory)

Devices
-------

Count  — number of computers with this device installed,
Probe  — latest probe ID of this device.

### Cpu

| MFG        | Name                           | GHz  | Count | Probe      |
|------------|--------------------------------|------|-------|------------|
| Intel      | Core i5-3330S                  | 2.70 | 5     | 0DBBF3578B |
| Intel      | Atom D525                      | 1.80 | 4     | 3C0BD31D74 |
| Intel      | Core i3 550                    | 3.20 | 4     | 196FDD9B5C |
| Intel      | Pentium G2030                  | 3.00 | 4     | DFD5E03377 |
| Intel      | Core i3-4130                   | 3.40 | 4     | 78D39E18EF |
| Intel      | Core i5-6400T                  | 2.20 | 4     | 447E6C6392 |
| AMD        | Athlon II X4 615e              |      | 3     | 2449E93D67 |
| Intel      | Core i5-7200U                  | 2.50 | 3     | FF0EDA1857 |
| Intel      | Core i5-2400S                  | 2.50 | 3     | 2107FF99A5 |
| Intel      | Core i5-2430M                  | 2.40 | 3     | 86F7ED27D4 |
| Intel      | Pentium J2900                  | 2.41 | 3     | 7048174273 |
| Intel      | Core i5-3470T                  | 2.90 | 3     | 2FAFA2B1CE |
| Intel      | Pentium N3700                  | 1.60 | 3     | 60932B706C |
| Intel      | Celeron J3060                  | 1.60 | 3     | 21CA50C386 |
| AMD        | Phenom II N830 Triple-Core     |      | 2     | 9E22E7425D |
| AMD        | E2-3800 APU with Radeon HD ... |      | 2     | 0B7EB6DDFC |
| Intel      | Core i5-8250U                  | 1.60 | 2     | AAFE223352 |
| Intel      | Core 2 Duo E7600               | 3.06 | 2     | 52C09EB3EA |
| Intel      | Pentium Dual-Core E5400        | 2.70 | 2     | 60AE7E3358 |
| Intel      | Core i5 750                    | 2.67 | 2     | 63741AC6DC |
| Intel      | Core i3-2100                   | 3.10 | 2     | D44BEF08F5 |
| Intel      | Core i5-2500S                  | 2.70 | 2     | 74411E469D |
| Intel      | Core i7-2600                   | 3.40 | 2     | D5C9D589F2 |
| Intel      | Core i7-2670QM                 | 2.20 | 2     | 94032B7164 |
| Intel      | Pentium G645                   | 2.90 | 2     | E2E294AF95 |
| Intel      | Celeron 1037U                  | 1.80 | 2     | A6325FF467 |
| Intel      | Core i3-3220                   | 3.30 | 2     | BEDE9EC674 |
| Intel      | Core i3-3227U                  | 1.90 | 2     | 3D1389215D |
| Intel      | Core i5-4210H                  | 2.90 | 2     | 881CDBC376 |
| Intel      | Core i5-5287U                  | 2.90 | 2     | 2146FDE0CF |
| Intel      | Core i5-4570R                  | 2.70 | 2     | 8F9240E65B |
| Intel      | Celeron N3150                  | 1.60 | 2     | 350CC83AC7 |
| Intel      | Pentium J4205                  | 1.50 | 2     | 82BBE8515C |
| AMD        | Athlon II X3 400e              |      | 1     | 1112BF63F0 |
| AMD        | Athlon II X3 420e              |      | 1     | 37DB8415D6 |
| AMD        | Athlon II X2 250u              |      | 1     | A7CE72E3F1 |
| AMD        | Turion II Ultra Dual-Core M... |      | 1     | 7EF4233E74 |
| AMD        | Athlon II X2 220               |      | 1     | AD16F12938 |
| AMD        | A4-3420 APU with Radeon HD ... |      | 1     | 973EEE4869 |
| AMD        | A6-3620 APU with Radeon HD ... |      | 1     | 0BB1CDF16A |
| AMD        | E-350                          |      | 1     | 70CA845CDF |
| AMD        | E-450 APU with Radeon HD Gr... |      | 1     | 8F09E8179C |
| AMD        | E1-1200 APU with Radeon HD ... |      | 1     | D3734872B2 |
| AMD        | E2-1800 APU with Radeon HD ... |      | 1     | 5DC25F1C8F |
| AMD        | A10-9700E RADEON R7, 10 COM... |      | 1     | 822331563B |
| AMD        | A6-9210 RADEON R4, 5 COMPUT... |      | 1     | C3AA8C3D72 |
| AMD        | A9-9400 RADEON R5, 5 COMPUT... |      | 1     | 42A88949A0 |
| AMD        | A9-9410 RADEON R5, 5 COMPUT... |      | 1     | E1331356C7 |
| AMD        | A9-9425 RADEON R5, 5 COMPUT... |      | 1     | 2B0CDC9F2B |
| AMD        | E2-9000 RADEON R2, 4 COMPUT... |      | 1     | 8ED375662D |
| AMD        | A10-7800 Radeon R7, 12 Comp... |      | 1     | 58DDD6F565 |
| AMD        | A8-7410 APU with AMD Radeon... |      | 1     | EC25F6A0A6 |
| AMD        | E1-6010 APU with AMD Radeon... |      | 1     | 728A63CA66 |
| AMD        | Ryzen 5 2400GE with Radeon ... |      | 1     | 65B33FAD81 |
| Intel      | Genuine T2500                  | 2.00 | 1     | BEF4BEA5F8 |
| Intel      | Core i3-7100U                  | 2.40 | 1     | 30323EE603 |
| Intel      | Core 2 T7400                   | 2.16 | 1     | 5E0E0CDD48 |
| Intel      | Core i5-8400T                  | 1.70 | 1     | 05D6B79D2F |
| Intel      | Celeron G3930T                 | 2.70 | 1     | 9A305C10EC |
| Intel      | Core i3-7100                   | 3.90 | 1     | D77183679A |
| Intel      | Core i7-7700HQ                 | 2.80 | 1     | EAF15D57C1 |
| Intel      | Core i7-7700T                  | 2.90 | 1     | AC62725ABE |
| Intel      | Pentium G4560T                 | 2.90 | 1     | ADFDA7721C |
| Intel      | Core 2 Duo E7500               | 2.93 | 1     | 57AD94D04A |
| Intel      | Core 2 Duo E8135               | 2.66 | 1     | 39D4FD28B4 |
| Intel      | Core 2 Duo E8400               | 3.00 | 1     | BB8D550F36 |
| Intel      | Core 2 Quad Q8200              | 2.33 | 1     | 6838533F5E |
| Intel      | Pentium Dual-Core E5500        | 2.80 | 1     | 17AD880F72 |
| Intel      | Pentium Dual-Core E5800        | 3.20 | 1     | 55C2F8E26B |
| Intel      | Pentium Dual-Core E6500        | 2.93 | 1     | 86E8BBC133 |
| Intel      | Pentium Dual-Core E6700        | 3.20 | 1     | 16127F5B64 |
| Intel      | Pentium Dual-Core T4400        | 2.20 | 1     | C75EE453D6 |
| Intel      | Core 2 Duo E8135               | 2.40 | 1     | BB93D1DD97 |
| Intel      | Core 2 Duo E8335               | 2.66 | 1     | 4038508FFC |
| Intel      | Atom D510                      | 1.66 | 1     | E0910009B1 |
| Intel      | Core i7 860                    | 2.80 | 1     | 3FDB026301 |
| Intel      | Core i5 650                    | 3.20 | 1     | CBD910EA06 |
| Intel      | Core i3 540                    | 3.07 | 1     | 68671C6971 |
| Intel      | Core i3 M 370                  | 2.40 | 1     | 777FB79E9C |
| Intel      | Core i3 M 380                  | 2.53 | 1     | E4BDC45558 |
| Intel      | Celeron 847                    | 1.10 | 1     | 4E9A1228DF |
| Intel      | Celeron G550                   | 2.60 | 1     | ADD51B1FFA |
| Intel      | Core i3-2120                   | 3.30 | 1     | F744394A1C |
| Intel      | Core i3-2310M                  | 2.10 | 1     | A130539775 |
| Intel      | Core i3-2370M                  | 2.40 | 1     | 5BB4C2C7A5 |
| Intel      | Core i5-2320                   | 3.00 | 1     | 0C3E26BCE7 |
| Intel      | Core i5-2410M                  | 2.30 | 1     | A43F128108 |
| Intel      | Core i7-2600S                  | 2.80 | 1     | 00CA2ACC7C |
| Intel      | Pentium G630                   | 2.70 | 1     | B98E1F49EA |
| Intel      | Celeron J1800                  | 2.41 | 1     | 5F6FF0EBDA |
| Intel      | Celeron J1900                  | 1.99 | 1     | E745E03926 |
| Intel      | Celeron N2830                  | 2.16 | 1     | 4279D783B3 |
| Intel      | Celeron N2840                  | 2.16 | 1     | E538C8D692 |
| Intel      | Celeron 1017U                  | 1.60 | 1     | 807B68E120 |
| Intel      | Celeron G1620                  | 2.70 | 1     | 63636F3FB9 |
| Intel      | Core i3-3240                   | 3.40 | 1     | EFDACF8C18 |
| Intel      | Core i5-3210M                  | 2.50 | 1     | 7685534B8E |
| Intel      | Core i5-3470S                  | 2.90 | 1     | 8DDD1670A8 |
| Intel      | Core i5-3570T                  | 2.30 | 1     | 16D6FCA3F1 |
| Intel      | Core i7-3610QE                 | 2.30 | 1     | 81CF385125 |
| Intel      | Core i7-3630QM                 | 2.40 | 1     | 5A1E3BCEB6 |
| Intel      | Core i7-3770S                  | 3.10 | 1     | 0633624824 |
| Intel      | Core i7-3770T                  | 2.50 | 1     | 857C8BE5D0 |
| Intel      | Core i3-4160T                  | 3.10 | 1     | 679675EE54 |
| Intel      | Core i5-4210M                  | 2.60 | 1     | 3FEDAA45BD |
| Intel      | Core i5-4460T                  | 1.90 | 1     | DA91B5ED2D |
| Intel      | Core i5-4570                   | 3.20 | 1     | 3DB357276B |
| Intel      | Core i7-4785T                  | 2.20 | 1     | 3321732E8E |
| Intel      | Pentium G3240T                 | 2.70 | 1     | 39F51C9AA1 |
| Intel      | Core i3-5005U                  | 2.00 | 1     | E7395D0EE2 |
| Intel      | Core i3-4010U                  | 1.70 | 1     | 93F51B8717 |
| Intel      | Celeron N3050                  | 1.60 | 1     | 2590C16EF0 |
| Intel      | Pentium J3710                  | 1.60 | 1     | C64BB03524 |
| Intel      | Pentium N3710                  | 1.60 | 1     | D2834ADB05 |
| Intel      | Celeron J3355                  | 2.00 | 1     | 3C8F739B30 |
| Intel      | Core i3-6100                   | 3.70 | 1     | 9704A885AC |
| Intel      | Core i3-6100T                  | 3.20 | 1     | 70E8C27FFF |
| Intel      | Core i5-6300HQ                 | 2.30 | 1     | 0D7DDE5C4F |
| Intel      | Core i5-6500                   | 3.20 | 1     | 63FB881055 |
| Intel      | Pentium G4400                  | 3.30 | 1     | 2281502A3C |
| Intel      | Pentium G4400T                 | 2.90 | 1     | 797B5C4507 |

### Memory

| MFG        | Name               | Size     | Type | MT/s | Count | Probe      |
|------------|--------------------|----------|------|------|-------|------------|
|            | Module DIMM        | 2048 MB  | DDR2 | 800  | 7     | 55C2F8E26B |
|            | Module             | 4096 MB  | DDR3 |      | 7     | A43F128108 |
| SK Hynix   | HMT351S6CFR8C-P... | 4096 MB  | DDR3 | 1600 | 6     | BAA91679D8 |
| SK Hynix   | HMT451S6AFR8A-P... | 4096 MB  | DDR3 | 1600 | 5     | 89E79AAB3C |
| SK Hynix   | HMT451S6BFR8A-P... | 4096 MB  | DDR3 | 1600 | 5     | 728A63CA66 |
| Kingston   | ACR128X64D3U133... | 1024 MB  | DDR3 | 1333 | 5     | 16127F5B64 |
| Kingston   | ACR256X64D3U133... | 2048 MB  | DDR3 | 1333 | 5     | 37DB8415D6 |
| SK Hynix   | HMA851S6AFR6N-U... | 4096 MB  | DDR4 | 2667 | 5     | EAF15D57C1 |
| SK Hynix   | HMT325S6CFR8C-P... | 2048 MB  | DDR3 | 1600 | 4     | 63636F3FB9 |
| Ramaxel    | RMT3170MN68F9F1... | 4096 MB  | DDR3 | 1600 | 4     | C64BB03524 |
| Ramaxel    | RMT3160ED58E9W1... | 4096 MB  | DDR3 | 1600 | 3     | 0DBBF3578B |
| Samsung    | M471A5244CB0-CR... | 4096 MB  | DDR4 | 2400 | 3     | 42A88949A0 |
| Samsung    | M471B5173QH0-YK... | 4096 MB  | DDR3 | 1600 | 3     | A6325FF467 |
| Samsung    | Module SODIMM      | 2048 MB  | DDR3 | 1067 | 3     | 3FDB026301 |
| SK Hynix   | Module SODIMM      | 4096 MB  | DDR3 | 1600 | 3     | 8F9240E65B |
| Unifosa    | GU512303EP0202 ... | 2048 MB  | DDR3 | 1333 | 3     | 6838533F5E |
| Crucial    | CT51264BF160BJ.... | 4096 MB  | DDR3 | 1600 | 2     | E745E03926 |
| Goldkey    | BKH400SO51208-1... | 4096 MB  | DDR3 | 1333 | 2     | 78D39E18EF |
| SK Hynix   | HMT41GS6BFR8A-P... | 8192 MB  | DDR3 | 1600 | 2     | 881CDBC376 |
| SK Hynix   | HMT451S6BFR8A-P... | 4096 MB  | DDR3 | 1600 | 2     | 2590C16EF0 |
| SK Hynix   | HMT325S6BFR8C-H... | 2048 MB  | DDR3 | 1333 | 2     | B98E1F49EA |
| Kingston   | ACR16D3LS1KFG/4... | 4096 MB  | DDR3 | 1600 | 2     | E538C8D692 |
| Kingston   | Module DIMM        | 4096 MB  | DDR3 | 1333 | 2     | 74411E469D |
| Micron     | Module DIMM        | 1024 MB  | DDR2 | 667  | 2     | BEF4BEA5F8 |
|            | Module SDRAM       | 2048 MB  |      |      | 2     | 17AD880F72 |
|            | Module             | 8192 MB  | DDR3 |      | 2     | 7685534B8E |
| Ramaxel    | RMT3150ED58E8W1... | 2048 MB  | DDR3 | 1600 | 2     | 0DBBF3578B |
| Ramaxel    | RMT3170MP68F9F1... | 4096 MB  | DDR3 | 1600 | 2     | 3C8870F1A3 |
| Samsung    | M471A1K43BB0-CP... | 8192 MB  | DDR4 | 2133 | 2     | 447E6C6392 |
| Samsung    | M471A5143EB1-CR... | 4096 MB  | DDR4 | 2400 | 2     | D77183679A |
| Samsung    | M471B5173BH0-CK... | 4096 MB  | DDR3 | 1600 | 2     | 16D6FCA3F1 |
| Samsung    | M471B5173DB0-YK... | 4096 MB  | DDR3 | 1600 | 2     | 93F51B8717 |
| Samsung    | M471B5273CH0-CH... | 4096 MB  | DDR3 | 1334 | 2     | F92CB57F54 |
| Samsung    | M471B5273EB0-CK... | 4096 MB  | DDR3 | 1600 | 2     | 53C511F523 |
| Samsung    | Module SODIMM      | 2048 MB  | DDR3 | 1333 | 2     | 2449E93D67 |
| SK Hynix   | HMA81GS6AFR8N-U... | 8192 MB  | DDR4 | 2400 | 2     | FF0EDA1857 |
| SK Hynix   | HMT451S6AFR8A-P... | 4096 MB  | DDR3 | 1600 | 2     | 807B68E120 |
|            | Module SODIMM      | 8192 MB  | DDR3 | 1333 | 1     | D5C9D589F2 |
|            | 323034383733383... | 2048 MB  | DDR2 | 667  | 1     | E0910009B1 |
| A-DATA     | 41444F564631423... | 2048 MB  | DDR2 | 667  | 1     | E0910009B1 |
| A-DATA     | AM1U139C2P13YV ... | 2048 MB  | DDR3 | 1067 | 1     | E4BDC45558 |
| A-DATA     | AM1U16BC4P2-B19... | 4096 MB  | DDR3 | 1600 | 1     | 857C8BE5D0 |
| Corsair    | Module FB-DIMM     | 1024 MB  | DDR2 | 667  | 1     | 5E0E0CDD48 |
| Crucial    | CT25664BF160B.C... | 2048 MB  | DDR3 | 1600 | 1     | 4E9A1228DF |
| Crucial    | CT51264BF160B.C... | 4096 MB  | DDR3 | 1600 | 1     | D2834ADB05 |
| Crucial    | Module SODIMM      | 4096 MB  | DDR3 | 1067 | 1     | 63741AC6DC |
| Crucial    | Module SODIMM      | 4096 MB  | DDR3 | 1333 | 1     | 2107FF99A5 |
| Elpida     | EBJ21UE8BFU0-DJ... | 2048 MB  | DDR3 | 1334 | 1     | 5BB4C2C7A5 |
| Elpida     | EBJ40UG8BBU0-GN... | 4096 MB  | DDR3 | 1600 | 1     | BAA91679D8 |
| Elpida     | EBJ41UF8BCS0-DJ... | 4096 MB  | DDR3 | 1334 | 1     | 728A63CA66 |
| Elpida     | Module SODIMM      | 2048 MB  | DDR3 | 1333 | 1     | 7F1ABC90BD |
| Goldkey    | GKH400SO51208-1... | 4096 MB  | DDR3 | 1600 | 1     | E89226495D |
| GOODRAM    | GR1600S364L11/8... | 8192 MB  | DDR3 | 1600 | 1     | E2E294AF95 |
| SK Hynix   | HMA851S6AFR6N-U... | 4096 MB  | DDR4 | 2400 | 1     | 822331563B |
| SK Hynix   | HMT425S6AFR6A-P... | 2048 MB  | DDR3 | 1600 | 1     | 7048174273 |
| SK Hynix   | HMA451S6AFR8N-T... | 4096 MB  |      | 2133 | 1     | 2281502A3C |
| SK Hynix   | HMA851S6AFR6N-U... | 4096 MB  | DDR4 | 2400 | 1     | 8ED375662D |
| SK Hynix   | HMT325S6EFR8C-P... | 2048 MB  | DDR3 | 1600 | 1     | 81CF385125 |
| SK Hynix   | HMT351S6CFR8C-H... | 4096 MB  | DDR3 | 1333 | 1     | D3734872B2 |
| SK Hynix   | HMT425S6CFR6A-PB   | 2048 MB  | DDR3 | 1600 | 1     | D40CB39503 |
| SK Hynix   | Module DIMM        | 4096 MB  | DDR3 | 1600 | 1     | DA91B5ED2D |
| SK Hynix   | HMT351S6BFR8C-H... | 4096 MB  | DDR3 | 1333 | 1     | 8F09E8179C |
| SK Hynix   | HMT425S6AFR6A-P... | 2048 MB  | DDR3 | 1600 | 1     | 83C2FE7761 |
| SK Hynix   | HMT451S6AFR8A-P... | 4096 MB  | DDR3 | 1600 | 1     | 83C2FE7761 |
| Kingston   | 99U5402-002.A01... | 1024 MB  |      | 1067 | 1     | 86E8BBC133 |
| Kingston   | 99U5469-046.A00... | 4096 MB  | DDR3 | 1333 | 1     | 78D39E18EF |
| Kingston   | 99U5469-053.A00... | 4096 MB  | DDR3 | 1333 | 1     | 8F9192D6D5 |
| Kingston   | 99U5469-054.A00... | 4096 MB  | DDR3 | 1333 | 1     | 973EEE4869 |
| Kingston   | 99U5474-010.A00... | 2048 MB  | DDR3 | 1333 | 1     | 196FDD9B5C |
| Kingston   | ACR16D3LS1KBG/4... | 4096 MB  | DDR3 | 1600 | 1     | 3D1389215D |
| Kingston   | ACR16D3LS1KDGR/... | 4096 MB  | DDR3 | 1600 | 1     | 21CA50C386 |
| Kingston   | ACR16D3LS1KNG/4... | 4096 MB  | DDR3 | 1600 | 1     | E7395D0EE2 |
| Kingston   | ACR512X64D3S16C... | 4096 MB  | DDR3 | 1600 | 1     | 0BB1CDF16A |
| Kingston   | KHX1600C9S3L/4G... | 4096 MB  | DDR3 | 1600 | 1     | 9CD47FB17E |
| Kingston   | KHX1866C11S3L/8... | 8192 MB  | DDR3 | 1867 | 1     | EC25F6A0A6 |
| Kingston   | 99U5594-002.A00... | 2048 MB  | DDR3 | 1333 | 1     | 5BB4C2C7A5 |
| Micron     | 16HTF51264HZ-80... | 4096 MB  | DDR2 | 800  | 1     | 7EF4233E74 |
| Micron     | 16KTF1G64HZ-1G6... | 8192 MB  | DDR3 | 1600 | 1     | 3321732E8E |
| Micron     | 4ATF51264HZ-2G3... | 4096 MB  | DDR4 | 2400 | 1     | C3AA8C3D72 |
| Micron     | 4KTF25664HZ-1G6... | 2048 MB  | DDR3 | 1600 | 1     | E534D33394 |
| Micron     | Module SODIMM      | 4096 MB  | DDR3 | 1600 | 1     | 3DB357276B |
| Nanya      | NT2GC64B88B0NS-... | 2048 MB  | DDR3 | 1334 | 1     | 9D54CA5AE9 |
| Nanya      | NT2GC64B88G0NS-... | 2048 MB  | DDR3 | 1600 | 1     | D3734872B2 |
| Nanya      | NT2GC64B8HC0NS-... | 2048 MB  | DDR3 | 1066 | 1     | 1112BF63F0 |
| Nanya      | NT4GC64B8HG0NS-... | 4096 MB  | DDR3 | 1333 | 1     | D973B3B4E4 |
| Nanya      | NT4GC64C88B1NS-... | 4096 MB  | DDR3 | 1600 | 1     | EFDACF8C18 |
| Nanya      | Module DIMM        | 2048 MB  | DDR2 | 800  | 1     | BB93D1DD97 |
|            | Module DIMM        | 1024 MB  | DDR2 | 800  | 1     | 60AE7E3358 |
|            | Module             | 1024 MB  | DDR3 | 1333 | 1     | 4143DA1BFB |
|            | Module             | 2048 MB  |      | 667  | 1     | 47822D532C |
|            | Module             | 2048 MB  |      | 800  | 1     | 9E22E7425D |
|            | Module             | 2048 MB  | DDR2 |      | 1     | 52C09EB3EA |
|            | Module DIMM        | 2048 MB  | DDR3 | 1066 | 1     | 777FB79E9C |
|            | Module SODIMM      | 2048 MB  | DDR3 | 1333 | 1     | 4143DA1BFB |
|            | Module             | 4096 MB  | DDR3 | 1066 | 1     | 777FB79E9C |
|            | Module             | 4096 MB  | DDR3 | 1866 | 1     | 82BBE8515C |
|            | Module SDRAM       | 4096 MB  |      | 1334 | 1     | BB8D550F36 |
| Ramaxel    | RMSA3270MB76H8F... | 2048 MB  | DDR4 | 2400 | 1     | E1331356C7 |
| Ramaxel    | RMSA3270MB86H9F... | 4096 MB  | DDR4 | 2400 | 1     | E1331356C7 |
| Ramaxel    | RMSA3270ME86H9F... | 4096 MB  | DDR4 | 2667 | 1     | 65B33FAD81 |
| Ramaxel    | RMT3170ME68F9F1... | 4096 MB  | DDR3 | 1600 | 1     | 39F51C9AA1 |
| Samsung    | M378B5673EH1-CH... | 2048 MB  | DDR3 | 1333 | 1     | 57AD94D04A |
| Samsung    | M378B5673FH0-CH... | 2048 MB  | DDR3 | 1600 | 1     | 196FDD9B5C |
| Samsung    | M378B5773CH0-CH... | 2048 MB  | DDR3 | 1867 | 1     | B3713978BF |
| Samsung    | M471A1K43CB1-CR... | 8192 MB  | DDR4 | 2667 | 1     | 9704A885AC |
| Samsung    | M471A2K43BB1-CP... | 16384 MB | DDR4 | 2133 | 1     | 63FB881055 |
| Samsung    | M471A5244BB0-CR... | 4096 MB  | DDR4 | 2400 | 1     | 2B0CDC9F2B |
| Samsung    | M471B1G73EB0-YK... | 8192 MB  | DDR3 | 1600 | 1     | 58DDD6F565 |
| Samsung    | M471B5673EH1-CH... | 2048 MB  | DDR3 | 1333 | 1     | 47822D532C |
| Samsung    | M471B5773DH0-CK... | 2048 MB  | DDR3 | 1600 | 1     | 9E22E7425D |
| Samsung    | Module DIMM        | 1024 MB  | DDR2 | 667  | 1     | 4038508FFC |
| Samsung    | Module DIMM        | 4096 MB  | DDR3 | 1333 | 1     | DFFA21B8C5 |
| SK Hynix   | HMA41GS6AFR8N-T... | 8192 MB  | DDR4 | 2133 | 1     | 0D7DDE5C4F |
| SK Hynix   | HMA81GS6CJR8N-U... | 8192 MB  | DDR4 | 2400 | 1     | AC62725ABE |
| SK Hynix   | HMA81GS6CJR8N-V... | 8192 MB  | DDR4 | 2667 | 1     | 05D6B79D2F |
| SK Hynix   | HMA81GS6MFR8N-U... | 8192 MB  | DDR4 | 2400 | 1     | AC62725ABE |
| SK Hynix   | HMT325S6CFR8C-P... | 2048 MB  | DDR3 | 1600 | 1     | 4E9A1228DF |
| SK Hynix   | HMT351S6CFR8C-P... | 4096 MB  | DDR3 | 1600 | 1     | A7CE72E3F1 |
| SK Hynix   | HMT451S6BFR8A-P... | 4096 MB  | DDR3 |      | 1     | 275E0EEAEF |
| SK Hynix   | KMKYF9-MIH SODIMM  | 8192 MB  | DDR4 | 2400 | 1     | 30D175ED83 |
| SK Hynix   | Module DIMM        | 1024 MB  | DDR2 | 667  | 1     | 4038508FFC |
| SK Hynix   | Module SODIMM      | 2048 MB  | DDR3 | 1067 | 1     | 63741AC6DC |
| SK Hynix   | Module DIMM        | 2048 MB  | DDR3 | 1333 | 1     | DFFA21B8C5 |
| Smart      | SH564128FH8NZPH... | 4096 MB  | DDR3 | 667  | 1     | 5DC25F1C8F |
| Transcend  | JM1333KSN-4G SO... | 4096 MB  | DDR3 | 1334 | 1     | F744394A1C |
| Transcend  | JM1333KSU-2G DIMM  | 2048 MB  | DDR3 | 1333 | 1     | F744394A1C |
| Unifosa    | GU502203EP0201 ... | 1024 MB  | DDR3 | 1333 | 1     | 57AD94D04A |
| Unifosa    | HU564403EP0200 ... | 4096 MB  | DDR3 | 1333 | 1     | D44BEF08F5 |
| Unifosa    | HU564403EP02000... | 4096 MB  |      | 1333 | 1     | BD0C627D01 |
| Unifosa    | HU6B4304EP0200 ... | 2048 MB  | DDR3 | 1067 | 1     | ADD51B1FFA |
| Unifosa    | HU6E4404EP0200 ... | 4096 MB  | DDR3 | 1600 | 1     | A11E971A0C |
| Unifosa    | Module SODIMM      | 4096 MB  | DDR3 | 1333 | 1     | 114C724C7E |
| Unifosa    | Module SODIMM      | 4096 MB  | DDR3 | 1600 | 1     | BEDE9EC674 |

